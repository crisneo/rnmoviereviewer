import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image, TextInput, FlatList,Button, Alert, ActivityIndicator, WebView
} from 'react-native';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import { MetaCriticService } from '../services/metacriticService';

export class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = { movies:[
            {
              "SearchItems": [
               
              ]
            }
          ], showLoader:false, movieToSearch:''};
            
    }
    moveToAbout(){
        this.props.navigation.navigate('AboutPage');
    }
    findMetascoreRating(contents){
        debugger;
        let indexPositive = contents.indexOf("metascore_w medium movie positive");
        let indexMixed = contents.indexOf("metascore_w medium movie mixed");
        let indexNegative = contents.indexOf("metascore_w medium movie negative");
        if(indexPositive!=-1){
            return contents.substring(indexPositive, 5);
        }else if(indexMixed!=-1){
            return contents.substring(indexMixed, 5);
        }else if(indexNegative!=-1){
            return contents.substring(indexNegative, 5);
        }else{
            return 0;
        }
    }
    setStateAsync(state) {
        return new Promise((resolve) => {
          this.setState(state, resolve)
        });
      }
    async searchMovie(){
        
        if(this.state.movieToSearch!=''){
            this.setState({showLoader:true});
            let movieToSearch = this.state.movieToSearch; 
            let movies = await MetaCriticService.getMovies(movieToSearch);
            let h = 0;           
            /*this.setState({
                movies: movies
            });
            this.setState({showLoader:false});*/
            await this.setStateAsync({movies: movies});
            await this.setStateAsync({showLoader:false});
        }
        
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.box, styles.box1]}>
                    <TextInput
                            editable = {true}
                            maxLength = {40} style={styles.mtextbox}
                            onChangeText={(text) => this.setState({movieToSearch:text})}
                            value={this.state.movieToSearch}
                        />
                    <Button
                        title="Learn More"
                        color="#841584"
                        accessibilityLabel="Learn more about this purple button" style={styles.mbutton}
                         onPress={()=>{this.searchMovie()}} />
                </View>
                <View style={[styles.box, styles.box2]} >
                <ActivityIndicator size="large"  animating={this.state.showLoader} style={[{height: 80}]} 
                        hidesWhenStopped={true} />
                <FlatList data={this.state.movies[0].SearchItems} renderItem={({item})=>
                    <View style={{flexDirection:'row', flex:1,backgroundColor:'red'}}>
                         {/*<Image
                            style={{width: 100, height: 100}}
                            source={{uri: 'http://static.metacritic.com/images/products/movies/3/bece7a47fc672eec886dba5a6189afbb-78.jpg'}}
                            />*/}
                        <View style={{flex:1, flexDirection:'column'}}>
                        <Text>{item.Title}</Text>
                        {/*<Text>{item.ReleaseDate}</Text>*/}
                        <WebView 
                        style={styles.WebViewStyle}  
                        javaScriptEnabled={true}
                        domStorageEnabled={true} 
                        source={{ html: item.ReleaseDate }}
                        />
                        {/*<Text>Rating: {item.Rating.CriticRating}</Text>*/}
                        <Text>Metascore rating: {()=>{this.findMetascoreRating(item.ReleaseDate)}}</Text>
                   </View>
                    </View>
                } />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column'
    },
    box: {
      /*height: 20 //set this one*/
    },
    box1: {
      backgroundColor: '#2196F3',
      height: '10%',
      flexDirection: 'row'
    },
    box2: {
      backgroundColor: '#8BC34A',
      height:'90%'
    },
    box3: {
      backgroundColor: '#e3aa1a'
    },
    mtextbox:{
        width:'80%'
    },
    mbutton:{
        width:'20%'
    },
    WebViewStyle:
    {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
    marginTop: (Platform.OS) === 'ios' ? 20 : 0,
    height:300
    }

  });
