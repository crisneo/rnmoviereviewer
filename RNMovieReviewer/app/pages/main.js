import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image, TextInput, FlatList, Button
} from 'react-native';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import { SearchPage } from '../pages/search';
import { AboutView } from '../pages/about';


const PagesStack = createSwitchNavigator({
    SearchPage: SearchPage,
    AboutPage: AboutView
},
{
  initialRouteName: 'SearchPage',
});

export class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <PagesStack />
        );
    }
}

