import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

export class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    componentDidMount(){
       setTimeout(() => {
            this.props.navigation.navigate('MainScreen');
        }, 10000);
    }
    render() {
        return (
            <Image
            style={{width: 400, height: 686}}
            source={require('../assets/KML23.png')}
          />
        );
    }
}
