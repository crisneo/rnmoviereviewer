
export const MetaCriticService = {
    getMovies: async function(movieToSearch){
        if(movieToSearch!=''){                        
            const response = await fetch("https://api-marcalencc-metacritic-v1.p.mashape.com/search/"+movieToSearch+"/movie?limit=20&offset=1",{ 
            method: 'GET', 
            headers: new Headers({
              'X-Mashape-Key': 'QtVZjWXNRxmshZpxHAUSe8IajBG6p1g3hqKjsn1c2t7o25o96I', 
              'Accept': 'application/json'
            })
          });
        const responseJson = await response.json();
        return responseJson;       
        }
    }
}