export class MovieInfo{
    constructor(id, title, data, rating) {
     this.id=id;
     this.title=title;
     this.data=data;
     this.rating=rating;   
    }

    getId(){
        return this.id;
    }
    getTitle(){
        return this.title;
    }
    getData(){
        return this.data;
    }
    getRating(){
        return this.rating;
    }
    
}
